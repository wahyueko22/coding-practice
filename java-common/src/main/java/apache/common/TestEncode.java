package apache.common;

import org.apache.commons.codec.binary.Base64;

public class TestEncode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String originalInput = "<![CDATA[<CUSTOMERS xsi:noNamespaceSchemaLocation=\"Schema_BCA_eForm.xsd\"\r\n" + 
				"				xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n" + 
				"				<CUSTOMER>\r\n" + 
				"					<CUSTOMER_DESKRIPSI>\r\n" + 
				"						<CUSTOMER_NAMA>Stanley Sutedy</CUSTOMER_NAMA>\r\n" + 
				"						<CUSTOMER_ALIAS/>\r\n" + 
				"						<CUSTOMER_BERTINDAK_SEBAGAI>000</CUSTOMER_BERTINDAK_SEBAGAI>\r\n" + 
				"						<CUSTOMER_BERTINDAK_SEBAGAI_LAINNYA/>\r\n" + 
				"						<CUSTOMER_REKENING_UNTUK>Pribadi</CUSTOMER_REKENING_UNTUK>\r\n" + 
				"						<CUSTOMER_NOMOR/>\r\n" + 
				"						<CUSTOMER_NO_REK_LAMA/>\r\n" + 
				"						<CUSTOMER_NO_REK_BARU/>\r\n" + 
				"						<CUSTOMER_CABANG>0008 - KCU BANDUNG</CUSTOMER_CABANG>\r\n" + 
				"						<CUSTOMER_ALAMAT_1>Jl. Hidup baru GG. L No 10</CUSTOMER_ALAMAT_1>\r\n" + 
				"						<CUSTOMER_ALAMAT_2>Kelurahan mekar sari</CUSTOMER_ALAMAT_2>\r\n" + 
				"						<CUSTOMER_ALAMAT_3>Komplek kebagusan</CUSTOMER_ALAMAT_3>\r\n" + 
				"						<CUSTOMER_ALAMAT_KOTA>Jakarta</CUSTOMER_ALAMAT_KOTA>\r\n" + 
				"						<CUSTOMER_ALAMAT_KODEPOS>14420</CUSTOMER_ALAMAT_KODEPOS>\r\n" + 
				"						<CUSTOMER_ALAMAT_NO_TELEPON>64715487</CUSTOMER_ALAMAT_NO_TELEPON>\r\n" + 
				"						<CUSTOMER_ALAMAT_NO_FAX>647155996</CUSTOMER_ALAMAT_NO_FAX>\r\n" + 
				"						<CUSTOMER_ALAMAT_NO_HP>085693344774</CUSTOMER_ALAMAT_NO_HP>\r\n" + 
				"						<CUSTOMER_ALAMAT_EMAIL>stanley_92@ymail.com</CUSTOMER_ALAMAT_EMAIL>\r\n" + 
				"						<ALAMAT_KIRIM_1>Jl. Topaz raya no 19</ALAMAT_KIRIM_1>\r\n" + 
				"						<ALAMAT_KIRIM_2>Pondok kopi utara</ALAMAT_KIRIM_2>\r\n" + 
				"						<ALAMAT_KIRIM_3>Gading serpong</ALAMAT_KIRIM_3>\r\n" + 
				"						<ALAMAT_KIRIM_KOTA>Tangerang</ALAMAT_KIRIM_KOTA>\r\n" + 
				"						<ALAMAT_KIRIM_KODEPOS>12333</ALAMAT_KIRIM_KODEPOS>\r\n" + 
				"						<CUSTOMER_SUMBER_PENGHASILAN>1</CUSTOMER_SUMBER_PENGHASILAN>\r\n" + 
				"						<CUSTOMER_SUMBER_PENGHASILAN_LAINNYA/>\r\n" + 
				"						<CUSTOMER_TUJUAN_BUKA_REK>1</CUSTOMER_TUJUAN_BUKA_REK>\r\n" + 
				"						<CUSTOMER_TUJUAN_BUKA_REK_LAINNYA/>\r\n" + 
				"						<ALAMAT_KERJA_NAMA_KANTOR>PT. ABC, Tbk</ALAMAT_KERJA_NAMA_KANTOR>\r\n" + 
				"						<ALAMAT_KERJA_JABATAN>supervisor</ALAMAT_KERJA_JABATAN>\r\n" + 
				"						<ALAMAT_KERJA_BIDANG_USAHA>Perikanan</ALAMAT_KERJA_BIDANG_USAHA>\r\n" + 
				"						<ALAMAT_KERJA_1>Jl. Daan mogot KM 22</ALAMAT_KERJA_1>\r\n" + 
				"						<ALAMAT_KERJA_2>Pesing indah</ALAMAT_KERJA_2>\r\n" + 
				"						<ALAMAT_KERJA_3>kebagusan</ALAMAT_KERJA_3>\r\n" + 
				"						<ALAMAT_KERJA_KOTA>Jakarta</ALAMAT_KERJA_KOTA>\r\n" + 
				"						<ALAMAT_KERJA_KODEPOS>12312</ALAMAT_KERJA_KODEPOS>\r\n" + 
				"						<ALAMAT_KERJA_NO_TELEPON>648888964</ALAMAT_KERJA_NO_TELEPON>\r\n" + 
				"						<ALAMAT_KERJA_NO_FAX>648889956</ALAMAT_KERJA_NO_FAX>\r\n" + 
				"						<CUSTOMER_NPWP>I</CUSTOMER_NPWP>\r\n" + 
				"						<CUSTOMER_NPWP_NO>123412341353452</CUSTOMER_NPWP_NO>\r\n" + 
				"						<CUSTOMER_TANDA_PENGENAL>1</CUSTOMER_TANDA_PENGENAL>\r\n" + 
				"						<CUSTOMER_TANDA_PENGENAL_NO>3412341234123412</CUSTOMER_TANDA_PENGENAL_NO>\r\n" + 
				"						<CUSTOMER_TANDA_PENGENAL_BERLAKU_SAMPAI>2020-02-0400:00:00.0</CUSTOMER_TANDA_PENGENAL_BERLAKU_SAMPAI>\r\n" + 
				"						<CUSTOMER_JENIS_KELAMIN>L</CUSTOMER_JENIS_KELAMIN>\r\n" + 
				"						<CUSTOMER_TEMPAT_LAHIR>Jakarta</CUSTOMER_TEMPAT_LAHIR>\r\n" + 
				"						<CUSTOMER_TANGGAL_LAHIR>2013-02-05</CUSTOMER_TANGGAL_LAHIR>\r\n" + 
				"						<CUSTOMER_STATUS_KAWIN>1</CUSTOMER_STATUS_KAWIN>\r\n" + 
				"						<CUSTOMER_AGAMA>2</CUSTOMER_AGAMA>\r\n" + 
				"						<CUSTOMER_AGAMA_LAINNYA/>\r\n" + 
				"						<CUSTOMER_PEKERJAAN>1</CUSTOMER_PEKERJAAN>\r\n" + 
				"						<CUSTOMER_PEKERJAAN_LAINNYA/>\r\n" + 
				"						<CUSTOMER_GAJI_UTAMA>1</CUSTOMER_GAJI_UTAMA>\r\n" + 
				"						<CUSTOMER_GAJI_LAINNYA>1</CUSTOMER_GAJI_LAINNYA>\r\n" + 
				"						<CUSTOMER_GAJI_LAINNYA_DETAIL>25</CUSTOMER_GAJI_LAINNYA_DETAIL>\r\n" + 
				"						<CUSTOMER_GAJI_TOTAL>3</CUSTOMER_GAJI_TOTAL>\r\n" + 
				"						<CUSTOMER_KEWARGANEGARAAN>2</CUSTOMER_KEWARGANEGARAAN>\r\n" + 
				"						<CUSTOMER_KEWARGANEGARAAN_WNA_ASAL/>\r\n" + 
				"						<CUSTOMER_KITAS_KITAP>2</CUSTOMER_KITAS_KITAP>\r\n" + 
				"						<CUSTOMER_KITAS_KITAP_NO>45565465498797897987</CUSTOMER_KITAS_KITAP_NO>\r\n" + 
				"						<CUSTOMER_KITAS_KITAP_BERLAKU_SAMPAI>2021-02-18</CUSTOMER_KITAS_KITAP_BERLAKU_SAMPAI>\r\n" + 
				"						<CUSTOMER_PENDIDIKAN_TERAKHIR>2</CUSTOMER_PENDIDIKAN_TERAKHIR>\r\n" + 
				"						<CUSTOMER_STATUS_RUMAH>1</CUSTOMER_STATUS_RUMAH>\r\n" + 
				"						<CUSTOMER_NAMA_REF>pepi</CUSTOMER_NAMA_REF>\r\n" + 
				"						<CUSTOMER_NAMA_IBU_KANDUNG>Neni Ani</CUSTOMER_NAMA_IBU_KANDUNG>\r\n" + 
				"						<PRODUK_TIPE>110</PRODUK_TIPE>\r\n" + 
				"						<PRODUK_MATA_UANG>IDR</PRODUK_MATA_UANG>\r\n" + 
				"						<PRODUK_GIRO_MATA_UANG/>\r\n" + 
				"						<PRODUK_PASPOR_BCA_TIPE>1</PRODUK_PASPOR_BCA_TIPE>\r\n" + 
				"						<PRODUK_PASPOR_BCA_JENIS>1</PRODUK_PASPOR_BCA_JENIS>\r\n" + 
				"						<PRODUK_PASPOR_BCA_INSTANT_NO/>\r\n" + 
				"						<PRODUK_PASPOR_BCA_PETUNJUK_LAYAR>1</PRODUK_PASPOR_BCA_PETUNJUK_LAYAR>\r\n" + 
				"						<SETORAN_AWAL_JUMLAH/>\r\n" + 
				"						<SETORAN_AWAL_TERBILANG/>\r\n" + 
				"						<FASILITAS_KLIKBCA>0</FASILITAS_KLIKBCA>\r\n" + 
				"						<FASILITAS_KLIKBCA_USER_ID/>\r\n" + 
				"						<FASILITAS_BBP>0</FASILITAS_BBP>\r\n" + 
				"						<FASILITAS_BBP_BISNIS/>\r\n" + 
				"						<FASILITAS_BBP_PRIORITAS/>\r\n" + 
				"						<FASILITAS_KEYBCA_IB>0</FASILITAS_KEYBCA_IB>\r\n" + 
				"						<FASILITAS_KEYBCA_TIPE/>\r\n" + 
				"						<FASILITAS_KEYBCA_SN/>\r\n" + 
				"						<FASILITAS_MBCA>0</FASILITAS_MBCA>\r\n" + 
				"						<FASILITAS_MBCA_HP_1/>\r\n" + 
				"						<FASILITAS_MBCA_HP_2/>\r\n" + 
				"						<FASILITAS_NO_BCA_CARD/>\r\n" + 
				"						<FASILITAS_AKTIVASI_FIN_MBCA>0</FASILITAS_AKTIVASI_FIN_MBCA>\r\n" + 
				"						<FASILITAS_AUTODEBET>0</FASILITAS_AUTODEBET>\r\n" + 
				"						<FASILITAS_AUTODEBET_1_CARD/>\r\n" + 
				"						<FASILITAS_AUTODEBET_1_CARD_TIPE/>\r\n" + 
				"						<FASILITAS_AUTODEBET_1_CARD_BESAR_BAYAR/>\r\n" + 
				"						<FASILITAS_AUTODEBET_1_CARD_NO/>\r\n" + 
				"						<FASILITAS_AUTODEBET_2>0</FASILITAS_AUTODEBET_2>\r\n" + 
				"						<FASILITAS_AUTODEBET_2_PRODUK/>\r\n" + 
				"						<FASILITAS_AUTODEBET_2_NO/>\r\n" + 
				"						<FASILITAS_AUTODEBET_3>0</FASILITAS_AUTODEBET_3>\r\n" + 
				"						<FASILITAS_AUTODEBET_3_PRODUK/>\r\n" + 
				"						<FASILITAS_AUTODEBET_3_NO/>\r\n" + 
				"						<FASILITAS_AUTODEBET_4>0</FASILITAS_AUTODEBET_4>\r\n" + 
				"						<FASILITAS_AUTODEBET_4_PRODUK/>\r\n" + 
				"						<FASILITAS_AUTODEBET_4_NO/>\r\n" + 
				"						<KUASA_DEBET_REK_NO/>\r\n" + 
				"						<KUASA_DEBET_REK_ATAS_NAMA/>\r\n" + 
				"						<REK_GABUNGAN_NAMA/>\r\n" + 
				"						<REK_GABUNGAN_NO_TANDA_PENGENAL/>\r\n" + 
				"						<REK_GABUNGAN_NO_CUSTOMER/>\r\n" + 
				"						<REK_GABUNGAN_STATUS_REK/>\r\n" + 
				"						<REK_GABUNGAN_NAMA_TERCETAK_PADA_KARTU/>\r\n" + 
				"						<PERNYATAAN_2_SETUJU_KETENTUAN_PRODUK>1</PERNYATAAN_2_SETUJU_KETENTUAN_PRODUK>\r\n" + 
				"						<PERNYATAAN_2_SETUJU_KETENTUAN_PRODUK_DETAIL>Tahapan\r\n" + 
				"						</PERNYATAAN_2_SETUJU_KETENTUAN_PRODUK_DETAIL>\r\n" + 
				"						<PERNYATAAN_2_SETUJU_KETENTUAN_FASILITAS_BCA>0</PERNYATAAN_2_SETUJU_KETENTUAN_FASILITAS_BCA>\r\n" + 
				"						<PERNYATAAN_2_SETUJU_KETENTUAN_FASILITAS_BCA_DETAIL/>\r\n" + 
				"						<PERNYATAAN_2_SETUJU_KETENTUAN_PASPOR_BCA>1</PERNYATAAN_2_SETUJU_KETENTUAN_PASPOR_BCA>\r\n" + 
				"						<PERNYATAAN_2_LAINNYA/>\r\n" + 
				"						<PERNYATAAN_2_LAINNYA_DETAIL/>\r\n" + 
				"						<PERNYATAAN_TEMPAT/>\r\n" + 
				"						<PERNYATAAN_TANGGAL>0000-00-00</PERNYATAAN_TANGGAL>\r\n" + 
				"						<VALIDASI_CATATAN_BANK_PROSES/>\r\n" + 
				"						<VALIDASI_CATATAN_BANK_DISETUJUI/>\r\n" + 
				"						<VALIDASI_CATATAN_BANK_NRT/>\r\n" + 
				"						<VALIDASI_CATATAN_BANK_CATATAN/>\r\n" + 
				"						<VALIDASI_CATATAN_BANK_KATEGORI_NASABAH/>\r\n" + 
				"						<KVDCB_ACCOUNT_TYPE/>\r\n" + 
				"						<KVDCB_KODE_PERORANGAN_BISNIS/>\r\n" + 
				"						<KVDCB_KODE_PEMBEBANAN_PAJAK/>\r\n" + 
				"						<KVDCB_GOLONGAN_PEMILIK/>\r\n" + 
				"						<KVDCB_BEBAN_BIAYA_ADMIN/>\r\n" + 
				"						<KVDCB_PERIODE_BIAYA_ADMIN/>\r\n" + 
				"						<KVDCB_PERIODE_BUNGA/>\r\n" + 
				"						<KVDCB_OD_PLAN/>\r\n" + 
				"						<KVDCB_PEJABAT_BANK/>\r\n" + 
				"						<KVDCB_KD_PENDUDUK_NONPENDUDUK/>\r\n" + 
				"						<KVDCB_WITHOLDING_PLAN/>\r\n" + 
				"						<KVDCB_SERVICE_CHARGE_CODE/>\r\n" + 
				"						<KVDCB_INTEREST_PLAN/>\r\n" + 
				"						<KVDCB_PERIODE_RK/>\r\n" + 
				"						<KVDCB_USER_CODE/>\r\n" + 
				"						<KVDCB_COST_CENTER/>\r\n" + 
				"						<KVDCB_KODE_NEGARA/>\r\n" + 
				"						<STATUS/>\r\n" + 
				"						<TANGGAL_ISI_FORM/>\r\n" + 
				"						<MAIL_CODE>1</MAIL_CODE>\r\n" + 
				"						<NO_REF>H47761</NO_REF>\r\n" + 
				"					</CUSTOMER_DESKRIPSI>\r\n" + 
				"				</CUSTOMER>\r\n" + 
				"			</CUSTOMERS>]]>";
		Base64 base64 = new Base64();
		String encodedString = new String(base64.encode(originalInput.getBytes()));
		System.out.println(encodedString);
		
		Object obJ = base64.encode(originalInput.getBytes());
		System.out.println(obJ.toString());
	}

}
