package inharitance;


	public class Bicycle
	{
	 // instance variables declared private
	    private int gear;
	    private int cadence;
	    private int speed;


	   //accessor (get) method for gear
	   public int getGear()
	   {
	       return gear;
	   }

	   //set method for gear
	    public boolean setGear(int gear)
	       {
	          if (gear >=1 && gear <= 3)
	          {
	              this.gear = gear;
	              return false;
	          }

	          else
	          {
	              System.out.println ("Sorry, please enter a number between 1-3");
	              return true;
	          }
	      }

	   //accessor (get) method for cadence
	   public int getCadence()
	   {
	       return cadence;
	   }

	   //set method for cadence
	   public boolean setCadence(int cadence)
	   {
	       if (cadence >=1 && cadence <=100)
	       {
	           this.cadence = cadence;
	           return false;
	       }
	       else
	         {
	            System.out.println ("Sorry, please enter a number between 1-100");
	            return true;
	         }


	   }

	   //accessor (get) method for speed
	   public int getSpeed()
	   {
	       if (gear == 1)
	       {
	           return cadence / 12;
	       }
	       else if (gear == 2)
	       {
	           return cadence / 6;
	       }
	       else if (gear == 3)
	       {
	           return cadence / 4;
	       }

	       else
	       {
	            return 0;
	       }


	  }

	} // end of main

