package test.json;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class TextJsonToJavaObject {
	
	public static String replaceResponse(Map<String,Object> mapRequest, String response) {
		System.out.println("response : " + response);
		String one = response.substring(0, 1);
		String jsonResponse = "";
		if ("[".equals(one)) {
			JSONArray jsonArrayInput = new JSONArray(response);
			JSONArray jsonArrayOutput = new JSONArray();
			Iterator<Object> iterator = jsonArrayInput.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArrayInput,jsonArrayOutput,mapRequest);
				} else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArrayInput,jsonArrayOutput, mapRequest);
				} else {
					jsonArrayOutput.put(next);
				}
			}
			jsonResponse =  jsonArrayOutput.toString();
			
		} else {
			JSONObject jsonObjectInput = new JSONObject(response);
			JSONObject jsonObjectOutput = new JSONObject();
			for (String key : jsonObjectInput.keySet()) {
				String keyStr = key;
				Object objectValue = jsonObjectInput.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, mapRequest);
				} else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, mapRequest);
				} else {
					if (mapRequest.containsKey(key)) {
						jsonObjectOutput.put(key, mapRequest.get(key));
					} else
						jsonObjectOutput.put(key, objectValue);
				}

			}
			jsonResponse =  jsonObjectOutput.toString();
		}
		
		System.out.println("output json = " +jsonResponse);
		return jsonResponse;
	}

	// jika anak adalah jsonobject dan parent adalah jsonarray
	private static void extractJsonObject(Object childJson, JSONArray parentJson, JSONArray newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONObject && parentJson instanceof JSONArray) {
			JSONObject jsonObjectInput = new JSONObject(childJson.toString());
			JSONObject jsonObjectOutput = new JSONObject();
			newJson.put(jsonObjectOutput);
			for (String key : jsonObjectInput.keySet()) {
				String keyStr = key;
				Object objectValue = jsonObjectInput.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else {
					if (newMapValue.containsKey(key)) {
						jsonObjectOutput.put(key, newMapValue.get(key));
					} else
						jsonObjectOutput.put(key, objectValue);
				}
			}
		}
	}

	// jika anak dan parent adalah ssama2 json object
	private static void extractJsonObject(String jsonKey, Object childJson, JSONObject parentJson, JSONObject newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONObject && parentJson instanceof JSONObject) {
			JSONObject jsonObjectInput = new JSONObject(childJson.toString());
			JSONObject jsonObjectOutput = new JSONObject();
			newJson.put(jsonKey, jsonObjectOutput);
			for (String key : jsonObjectInput.keySet()) {
				String keyStr = key;
				Object objectValue = jsonObjectInput.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				}
				else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else {
					if (newMapValue.containsKey(key)) {
						jsonObjectOutput.put(key, newMapValue.get(key));
					} else
						jsonObjectOutput.put(key, objectValue);
				}
			}
		}
	}

	// jika anak adalah jsonArray dan parent adalah jsonarray
	private static void extractJsonArray(Object childJson, JSONArray parentJson, JSONArray newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONArray && parentJson instanceof JSONArray) {
			JSONArray jsonArrayInput = new JSONArray(childJson.toString());
			JSONArray jsonArrayOuput = new JSONArray();
			newJson.put(jsonArrayOuput);
			Iterator<Object> iterator = jsonArrayInput.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				}
				else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				} else {
					jsonArrayOuput.put(next);
				}
			}
		}
	}

	// jika anak adalah jsonArray dan parent adalah jsonObject
	private static void extractJsonArray(String jsonKey, Object childJson, JSONObject parentJson, JSONObject newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONArray && parentJson instanceof JSONObject) {
			JSONArray jsonArrayInput = new JSONArray(childJson.toString());
			JSONArray jsonArrayOuput = new JSONArray();
			newJson.put(jsonKey, jsonArrayOuput);
			Iterator<Object> iterator = jsonArrayInput.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				}
				else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				} else {
					jsonArrayOuput.put(next);
				}
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String,Object> mapRequest = new HashMap<String, Object>();
		mapRequest.put("ErrorCode", "NO-000");
		mapRequest.put("Name", "JOHN");
		mapRequest.put("CardNumber", "CN0009");
		String response = "{\r\n" + 
				"   \"ErrorSchema\":{\r\n" + 
				"      \"ErrorCode\":\"TEST-00-000\",\r\n" + 
				"      \"ErrorMessage\":{\r\n" + 
				"         \"English\":\"Success\",\r\n" + 
				"         \"Indonesian\":\"Berhasil\"\r\n" + 
				"      }\r\n" + 
				"   },\r\n" + 
				"   \"OutputSchema\":{\r\n" + 
				"      \"Name\":\"Sven\",\r\n" + 
				"      \"CardNumber\":\"CN004\",\r\n" + 
				"      \"ExpiredParam\":\"3600\",\r\n" + 
				"      \"ExpiredDate\":\"2019-12-18 14:28:29\"\r\n" + 
				"   }\r\n" + 
				" }";
		replaceResponse(mapRequest, response);

	}

}
