package test.inter;

import coba.model.CustomerDesciption;

public abstract class Inter<Req,Res> implements InterTest<Req,Res>{

	abstract public void print(Req req, String res);

	abstract public <T> void printData(T t);

}
