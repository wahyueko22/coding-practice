package test.inter;

public interface InterTest<Req,Res> {
	void print(Req req, Res res);
	<T> void printData(T t);
}
