package test.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//https://medium.com/@mr.anmolsehgal/java-hashmap-internal-implementation-21597e1efec3
// hashtable will be the index array
// value will be key value


public class MainMap {
	
	static class Coba {
		private String key;

		public Coba(String key) {
			super();
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		/*
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			return result;
		}*/
		
		@Override
		public int hashCode() {
			Random r = new Random();
			return r.nextInt((1000 - 1) + 1) + 1;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Coba other = (Coba) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}

		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MainMap mainMap =  new MainMap();
		
		Coba co1 = new Coba("satu");
		Coba co2 = new Coba("satu");
		
		TestHashMap<String, String> testMapa = new TestHashMap<String, String>();
		testMapa.put("satu", "jakarta");
		testMapa.put("satu", "jepara");
		System.out.println(testMapa.get("satu"));

	}
	

}
