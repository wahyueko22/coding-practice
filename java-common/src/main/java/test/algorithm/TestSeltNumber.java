package test.algorithm;

public class TestSeltNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int total = 0;
		for (int i = 1; i < 5000; i++) {
			//divice by 2, because it is not possible, the generator less than half of generated number
			int startSecondLoop = i / 2;
			boolean ismatch = false;
			//looping from around half value generated number
			for (int j = startSecondLoop; j < i; j++) {
				String strNumber = Integer.toString(j);
				int result = j;
				for (int k = 0; k < strNumber.length(); k++) {
					result += Character.getNumericValue(strNumber.charAt(k));
				}
				if (result == i) {
					ismatch = true;
					break;
				}
			}
			if (!ismatch) {
				System.out.print(" " + i + " ,");
				total += i;
			}
			
		}
		System.out.println();
		System.out.println( "Total value = " + total);

	}

}
