package test.algorithm;

public class FaktorialOfNumber {
	
	static int faktorial(int num) {
		if (num == 0) {
			return 1;
		}
		return num * faktorial(num - 1);
	}
	
	public static void main(String[] args) {
		int a = faktorial(3);
		System.out.println(a);
		
	}

}
