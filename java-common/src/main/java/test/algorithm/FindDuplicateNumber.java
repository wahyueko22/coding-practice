package test.algorithm;

import java.util.HashSet;
import java.util.Set;

public class FindDuplicateNumber {	
	public static void main(String[] args) {
		int number = 2132387;
		Set<Character> lsSet = new HashSet<Character>();
		String strData = number + "";
		for(int i=0; i<strData.length(); i++) {
			int counter = 0;
			for(int j=0; j<strData.length(); j++) {
				if (strData.charAt(i) == strData.charAt(j))
					counter +=1;
			}
			
			if (counter > 1) {
				lsSet.add(strData.charAt(i));
			}
		}
		
		for (Character character : lsSet) {
			System.out.println(character);
		}
	}
}
