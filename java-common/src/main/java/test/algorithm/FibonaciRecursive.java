package test.algorithm;
import java.util.*;

public class FibonaciRecursive {

	static int fib(int n) {
		if (n <= 1)
			return n;
		return fib(n - 1) + fib(n - 2);
	}

	static void fiboRecursive2(int start1, int start2, int loopStart, int loopLimit) {
		if (loopStart <= loopLimit) {
			int result = start1 + start2;
			System.out.print(result + " , ");
			loopStart++;
			fiboRecursive2(start2, result, loopStart, loopLimit);
		}
	}

	static void ifRecursive(int a) {
		if (a > 5)
			return;
		System.out.println(a);
		ifRecursive(a += 1);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(fib(1)); 
		// System.out.print(0 +" , " + 1 + " , ");
		// fiboRecursive2(0, 1, 1,10);

		//ifRecursive(1);
	}

}
