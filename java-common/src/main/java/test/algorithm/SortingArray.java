package test.algorithm;

public class SortingArray {
	
	public static void sortArray() {
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arrInt = new int[] {6,4,5,1,8,9,3};
		for(int i=0; i< arrInt.length; i++) {
			for(int j=0; j < arrInt.length - i - 1; j++) {
				if(arrInt[j] > arrInt[j + 1]) {
					int temp = arrInt[j + 1];
					arrInt[j + 1] = arrInt[j];
					arrInt[j] = temp;
				}
			}
		}
		
		for (int i : arrInt) {
			System.out.println(i + " , ");
		}

	}

}
