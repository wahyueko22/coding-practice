package test.reflection.coba3.filter;



import java.lang.reflect.Field;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


public class ValidationFilter {

    private <T> boolean isPrimitive(Class<T> klass) {

        return klass.isPrimitive();
    }

    private <T> boolean isString(Class<T> klass) {

        return String.class == klass; //String is final therefor you can not extend it.

    }

    private <T> boolean isPrimitiveWrapper(Class<T> klass) {

        return Character.class == klass || Boolean.class == klass || klass.isAssignableFrom(Number.class);

    }

    public void validate(Object data) throws Exception {
        Field[] fields = data.getClass().getDeclaredFields();
        for (Field field : fields) {
            if(field.getAnnotations().length==0){
                continue;    
            }
            Class classType = field.getType();
            field.setAccessible(true);
            Object value = field.get(data);            
            if (isPrimitive(classType) || isString(classType) || isPrimitiveWrapper(classType)) {
                String fieldName = field.getAnnotation(Validation.class).FieldName()!=null?field.getAnnotation(Validation.class).FieldName():"";
                if (fieldName == null || fieldName.length() == 0) {
                    fieldName = field.getName().substring(0, 1).toUpperCase()+ field.getName().substring(1);
                }
                boolean isMandatory = field.getAnnotation(Validation.class).Mandatory();
                boolean isNumber = field.getAnnotation(Validation.class).IsNumber();
                boolean isDate = field.getAnnotation(Validation.class).IsDate();
                String formatNumber = field.getAnnotation(Validation.class).FormatNumber();
                String formatDate = field.getAnnotation(Validation.class).FormatDate();
                if (isMandatory) {
                    if (!isEmpty(value)) {
                        throw new Exception("Mandatory field :" + fieldName);
                    }
                }
                if (isNumber && formatNumber.equals("")) {
                    if (!isNumeric(value)) {
                        throw new Exception("Invalid Number field :" + fieldName);
                    }
                }
                if (isNumber && (formatNumber.length() > 0)) {
                    if (!isNumeric(formatNumber, value)) {
                        throw new Exception("Invalid Number field :" + fieldName);
                    }
                }
                if (isDate) {
                    if (!isDate(formatDate, value)) {
                        throw new Exception("Invalid Date field :" + fieldName);
                    }
                }
            } else {
                validate(value);
            }
        }
    }

    private boolean isEmpty(Object value) {
        if ((value == null) || (value.toString().length() == 0))
            return false;
        else
            return true;
    }

    private boolean isDate(String format, Object value) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            formatter.parse(value.toString());
        } catch (Exception pex) {
            return false;
        }
        return true;
    }

    private boolean isNumeric(Object value) {
        return value.toString().matches("-?\\d+(\\.\\d+)?");
    }

    private boolean isNumeric(String format, Object value) throws Exception {
        try {
            DecimalFormat decFormat = new DecimalFormat(format);
            decFormat.parse(value.toString());
        } catch (NumberFormatException nfex) {
            return false;
        }
        return true;
    }
}
