package test.reflection.coba3.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Validation{
    String FieldName() default "";
    boolean Mandatory() default false;
    boolean IsNumber() default false;
    boolean IsDate() default false;
    String FormatDate() default "yyyy-MM-dd'T'HH:mm:ss.SSS" ;
    String FormatNumber() default "";    
}