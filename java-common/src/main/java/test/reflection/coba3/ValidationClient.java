package test.reflection.coba3;

import test.reflection.coba3.filter.Validation;
import test.reflection.coba3.filter.ValidationFilter;

public class ValidationClient {

    @Validation(Mandatory = true)
    private String testMandatory;

    @Validation(Mandatory = true, IsDate = true, FormatDate = "yyyy-MM-dd")
    private String testDate;

    @Validation(Mandatory = true, IsNumber = true)
    private double testNumber;

    @Validation(Mandatory = true, IsNumber = true)
    private String testStrNumber;

    private String dataBebas;

    public void setTestMandatory(String testMandatory) {
        this.testMandatory = testMandatory;
    }

    public String getTestMandatory() {
        return testMandatory;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestNumber(double testNumber) {
        this.testNumber = testNumber;
    }

    public double getTestNumber() {
        return testNumber;
    }

    public void setTestStrNumber(String testStrNumber) {
        this.testStrNumber = testStrNumber;
    }

    public String getTestStrNumber() {
        return testStrNumber;
    }

    public void setDataBebas(String dataBebas) {
        this.dataBebas = dataBebas;
    }

    public String getDataBebas() {
        return dataBebas;
    }

    public static void main(String args[]) throws Exception {
        ValidationClient cli = new ValidationClient();
        cli.setTestMandatory("");
        cli.setTestStrNumber("1212122");
        cli.setTestDate("2019-07-2019");
        new ValidationFilter().validate(cli);
    }
}
