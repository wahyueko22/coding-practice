package test.reflection.coba2;

import java.lang.reflect.Method;

import test.reflection.coba1.CustomAnnotatedClass;

public class MyTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Class object = CustomAnnotatedClass.class; 

		AnnotationRunner runner = new AnnotationRunner();
        Method[] methods = AnnotationRunner.class.getMethods();
        for (Method method : methods) {
            CanRun annos = method.getAnnotation(CanRun.class);
            if (annos != null) {
                try {
                    method.invoke(runner);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
