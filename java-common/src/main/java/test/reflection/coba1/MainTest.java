package test.reflection.coba1;

import java.lang.annotation.Annotation;

public class MainTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Class object = CustomAnnotatedClass.class; 
		// Retrieve all annotations from the class 
		Annotation[] annotations = object.getAnnotations(); 
		for( Annotation annotation : annotations ) { 
			System.out.println(annotation); 
		} 
		
		// Checks if an annotation is present 
		if( object.isAnnotationPresent( CustomAnnotatedClass.class ) ) { 
		// Gets the desired annotation 
			Annotation annotation = object.getAnnotation(CustomAnnotatedClass.class) ; 
			System.out.println(annotation); 
		} 
		// fetch the attributes of the annotation
		/*
		 * for(Annotation annotation : annotations) { System.out.println("name: " +
		 * annotation.name()); System.out.println("Date of Birth: "+
		 * annotation.dateOfBirth()); }
		 */
		
		// the same for all methods of the class
		/*for (Method method : object.getDeclaredMethods()) {
			if (method.isAnnotationPresent(CustomAnnotationMethod.class)) {
				Annotation annotation = method.getAnnotation(CustomAnnotationMethod.class);
				System.out.println(annotation);
			}
		}*/

	}

}
