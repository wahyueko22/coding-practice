package test.arraylist;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayListConstruct {
	transient String[] elementData; 
	private static final String[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
	private int index;
	private int incrementCapacity = 2;
	
	public ArrayListConstruct() {
		this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
	}
	
	public void add(String item) {
		ensureExplicitCapacity();
		elementData[index++] = item;
		System.out.println(index + " and " + elementData.length);
	}
	
	public void ensureExplicitCapacity() {
		if(elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
			String[] tempData = new String[incrementCapacity];
			elementData = tempData;
		}
		else if(elementData.length <=  index) {
			String[] tempData = new String[index + incrementCapacity];
			for (int i = 0; i< elementData.length; i++) {
				tempData[i] = elementData[i];
			}
			elementData = tempData;
		}
	}
	
	public void remove(String item) {
		 for(int i = 0; i < elementData.length; i++){
	            if(elementData[i] == item){
	                // shifting elements
	                for(int j = i; j < elementData.length - 1; j++){
	                	elementData[j] = elementData[j+1];
	                }
	                
	                
	                break;
	            }
	        }
	}
	
	public boolean contain(String item) {
		for (String str : elementData) {
			if(item.equals(str)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	
	
	public static void main(String args[]) {
	
		ArrayListConstruct obj = new ArrayListConstruct();
		System.out.println(obj.elementData.length);
		obj.add("1");
		obj.add("2");
		obj.add("3");
		obj.add("4");
		obj.remove("2");
		if(obj.contain("1"))
			System.out.println("OK");
		for (String string : obj.elementData) {
			System.out.println(string);
		}
	}
}
