package test.lamda.jenkov;

public interface StateChangeListener {
	 public void onStateChange(String oldState, String newState);
}
