package test.lamda.jenkov;

public class MainState {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StateOwner stateOwner = new StateOwner();

		stateOwner.addStateListener(
		    (String oldState, String newState) -> System.out.println("State changed")
		);
	}

}
