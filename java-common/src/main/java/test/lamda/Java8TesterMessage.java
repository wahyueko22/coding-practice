package test.lamda;

public class Java8TesterMessage {

	final static String salutation = "Hello! ";

	public static void main(String args[]) {
		GreetingService greetService1 = message -> System.out.println(salutation + message);
		greetService1.sayMessage("Mahesh");
		
		InterfaceMath interfaceMath = (a,b) -> a+b;

		int aa = interfaceMath.operasional(4, 5);
	}

	interface GreetingService {
		void sayMessage(String message);
	}
	
	interface InterfaceMath{
		int operasional(int a, int b);
	}

}
