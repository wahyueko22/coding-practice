package test.main;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

public class TestImage {
	
	private  static void testString(Object data) throws Exception {
		if(data instanceof String)
		 System.out.println(String.valueOf(data));
		else throw new Exception("error number");
	}
	
	//https://www.programcreek.com/java-api-examples/?api=org.imgscalr.Scalr
	private static BufferedImage getBestFit (BufferedImage bi, int maxWidth, int maxHeight)
	{
		if (bi == null)
			return null;

		Mode mode = Mode.AUTOMATIC;
		//int maxSize = Math.min(maxWidth, maxHeight);
		int maxSize = maxHeight;
		mode = Mode.FIT_TO_HEIGHT;
		return Scalr.resize(bi, Method.QUALITY, mode, maxSize, Scalr.OP_ANTIALIAS);
	}

	public static void main(String[] args) throws Exception  {
		// TODO Auto-generated method stub
		File image = new File("D://imageLocal//download3.jpg");
		BufferedImage thumb = ImageIO.read(image); // load image
		int width = (int) (thumb.getWidth() * 2);
		int height = (int) (thumb.getHeight() * 2);
		
		
		String newPath ="D://imageLocal//result//download3_res1.jpg";
		BufferedImage scaledImg = Scalr.resize(thumb, Mode.FIT_TO_HEIGHT, 0, 1000);
        File destFile = new File(newPath);
        ImageIO.write(scaledImg, "jpg", destFile);
        
        String newPath2 ="D://imageLocal//result//download3_res2.jpg";
		BufferedImage scaledImg2 = Scalr.resize(thumb, Mode.FIT_TO_HEIGHT,1000);
        File destFile2 = new File(newPath2);
        ImageIO.write(scaledImg2, "jpg", destFile2);
     //   TestImage.testString(5);
        String newPath3 ="D://imageLocal//result//download3_res3.jpg";
        File destFile3 = new File(newPath3);
        BufferedImage bestFitImage = Scalr.resize(thumb, Method.ULTRA_QUALITY, Mode.FIT_TO_HEIGHT, 1000, Scalr.OP_ANTIALIAS);
        ImageIO.write(bestFitImage, "jpg", destFile3);
        
       
	}
}
