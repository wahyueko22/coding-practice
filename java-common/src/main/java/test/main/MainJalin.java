package test.main;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.*;
import java.net.ServerSocket;
import java.net.Socket;

public class MainJalin {

	public static void main(String[] args) {
		
		// Register service on port 1254
		ServerSocket s;
		try {
			s = new ServerSocket(1254);
		
			Socket s1=s.accept(); // Wait and accept a connection
			// Get a communication stream associated with the socket
			OutputStream s1out = s1.getOutputStream();
			DataOutputStream dos = new DataOutputStream (s1out);
			// Send a string!
			dos.writeUTF("Hi there");
			// Close the connection, but not the server socket
			dos.close();
			s1out.close();
			s1.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

}
