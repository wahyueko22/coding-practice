package test.compiler;

import java.io.File;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

public class CompilingFromFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        int result = compiler.run(null, null, null,
                new File("Test.java").getAbsolutePath());
        if (result == 0) {
            System.out.println("compilation done");
        }

	}

}
