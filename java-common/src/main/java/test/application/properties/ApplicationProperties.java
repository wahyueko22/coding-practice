package test.application.properties;

import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {
	private final Properties properties;

    ApplicationProperties() {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("test.properties"));
        } catch (IOException e) {
            System.out.println(e.getMessage());;
        }
    }

    public String getAppName() {
        return properties.getProperty("app.name");
    }
    
    
}
