package test.token.jwt;

import io.jsonwebtoken.Claims;

public class MainToken {
	
	public static void main(String[] args) {
		JwtTokenUtil tkn = new JwtTokenUtil();
	//	String token = tkn.generateToken("wahyu");
	//	Claims claimRes = tkn.getAllClaimsFromToken(token);
	//	System.out.println(claimRes);
		
		TokenProvider pro = new TokenProvider();
		String proToken = pro.createToken("wahyu", "rempah", false);
		System.out.println(proToken);
		Claims claim = pro.getAuthentication(proToken);
		System.out.println(claim);
		System.out.println(claim.getExpiration());
	//	pro.validateToken(proToken);
	}
}
