package test.token.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.Key;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

//https://stackoverflow.com/questions/41661821/java-lang-illegalargumentexception-a-signing-key-must-be-specified-if-the-speci
public class JJWTDemo {

    private static final Key secret = MacProvider.generateKey();
    private static final byte[] secretBytes = secret.getEncoded();
    private static final String base64SecretBytes = Base64.getEncoder().encodeToString(secretBytes);

    private static String generateToken() {
        String id = UUID.randomUUID().toString().replace("-", "");
        Date now = new Date();
        Date exp = new Date(System.currentTimeMillis() + (1000 * 30)); // 30 seconds
        System.out.println(secret.getAlgorithm());
        String token = Jwts.builder()
            .setId(id)
            .setIssuedAt(now)
            .setNotBefore(now)
            .setExpiration(exp)
            .signWith(SignatureAlgorithm.HS256, base64SecretBytes)
           // .signWith(secret, SignatureAlgorithm.HS512)
            .compact();

        return token;
    }
    
    private static String generateTokenCustom() {
        String id = UUID.randomUUID().toString().replace("-", "");
      //  Date now = new Date();
        Map<String, Object> claims = new HashMap<>();
        claims.put("status", "nikah");
        claims.put("role", "approver");
        List<String> lsRole = new ArrayList<String>();
        lsRole.add("maker");
        lsRole.add("approver");
        Date exp = new Date(System.currentTimeMillis() + (30)); // 30 seconds
        System.out.println(secret.getAlgorithm());
        String token = Jwts.builder()
            .setId(id)
            .setSubject("wahyu")
            .setIssuedAt(new Date())
            .setIssuer("System")
            .claim("role",lsRole)
            //.setClaims(claims)
            .setExpiration(exp)
           // .signWith(SignatureAlgorithm.HS256, base64SecretBytes)
            .signWith(secret, SignatureAlgorithm.HS512)
            .compact();

        return token;
    }


    private static void verifyToken(String token) {
        Claims claims = Jwts.parser()
            .setSigningKey(base64SecretBytes)
            .parseClaimsJws(token).getBody();
        System.out.println("----------------------------");
        System.out.println("ID: " + claims);
        System.out.println("----------------------------");
        System.out.println("ID: " + claims.getId());
        Set<String> lsRoless = new HashSet<>();
        lsRoless.add("Maker");
        lsRoless.add("Approver");
        if(claims.get("role") instanceof Set) {
        	lsRoless =  (Set<String>) claims.get("role");
        }
        System.out.println("Subject: " + claims.getSubject());
        System.out.println("role: " + lsRoless);
        System.out.println("Issuer: " + claims.getIssuedAt());
        System.out.println("Expiration: " + claims.getExpiration());
    }

    public static void main(String[] args) throws InterruptedException {
       
        String token = generateTokenCustom();
        System.out.println(token);
        Thread.sleep(1000);
        try {
        	verifyToken(token);
        }catch (ExpiredJwtException e) {
			// TODO: handle exception
        	System.out.println("expired");
		}
    }
}