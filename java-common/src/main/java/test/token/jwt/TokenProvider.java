package test.token.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
//https://github.com/szerhusenBC/jwt-spring-security-demo/blob/master/src/main/java/org/zerhusen/security/jwt/TokenProvider.java
import java.util.Date;

public class TokenProvider {

	   private final Logger log = LoggerFactory.getLogger(TokenProvider.class);

	   private static final String AUTHORITIES_KEY = "auth";
	   
	   private String base64Secret = "mySecret";
	   //86400
	   private final long tokenValidityInMilliseconds=86400;
	   //108000
	   private final long tokenValidityInMillisecondsForRememberMe = 10000;

	   private static final Key key = MacProvider.generateKey();
	   
	   //jwt.header=Authorization
	  // jwt.secret=mySecret
		//	   jwt.expiration=604800
		//	   
	   public TokenProvider() {}

	   public String createToken(String name, String role, boolean rememberMe) {


	    //  long now = System.currentTimeMillis();
	      Date validity;
	      if (rememberMe) {
	         validity = new Date(System.currentTimeMillis() + this.tokenValidityInMillisecondsForRememberMe);
	      } else {
	         validity = new Date(System.currentTimeMillis() + this.tokenValidityInMilliseconds);
	      }

	      return Jwts.builder()
	         .setSubject(name)
	         .claim(AUTHORITIES_KEY, role)
	         .signWith(key, SignatureAlgorithm.HS512)
	         .setIssuedAt(new Date())
	         .setExpiration(validity)
	         .compact();
	   }

	   public Claims getAuthentication(String token) {
	      Claims claims = Jwts.parser()
	         .setSigningKey(key)
	         .parseClaimsJws(token)
	         .getBody();
	      return claims;
	   }

	   public boolean validateToken(String authToken) {
	      try {
	         Jwts.parser().setSigningKey(key).parseClaimsJws(authToken);
	         return true;
	      } catch (io.jsonwebtoken.security.SecurityException | MalformedJwtException e) {
	         log.info("Invalid JWT signature.");
	         log.trace("Invalid JWT signature trace: {}", e);
	      } catch (ExpiredJwtException e) {
	         log.info("Expired JWT token.");
	         log.trace("Expired JWT token trace: {}", e);
	      } catch (UnsupportedJwtException e) {
	         log.info("Unsupported JWT token.");
	         log.trace("Unsupported JWT token trace: {}", e);
	      } catch (IllegalArgumentException e) {
	         log.info("JWT token compact of handler are invalid.");
	         log.trace("JWT token compact of handler are invalid trace: {}", e);
	      }
	      return false;
	   }
	}