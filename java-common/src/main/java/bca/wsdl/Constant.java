package bca.wsdl;

public class Constant {
	public static final String spliterUrl = "/mockbca/";
	// utils
	public static final String defaultFormatDt = "yyyy-MM-dd";
	public static final String defaultFormatTimeStamp = "yyyy-MM-dd HH:mm";
	public static final String STRING_SAP_SUCCESS_CODE = "000";
	
	public static final String WSDL_SERVICE_MOCK = "WSDL";
	
	public static final String REQUEST_ONLINE_CODE = "1";
	public static final String REQUEST_OFFLINE_CODE = "2";
	
	public static final String RESPONSE_STRING_ERROR = "error";
	public static final String RESPONSE_STRING_SUCCESS = "success";
	public static final String RESPONSE_SUCCESS_CODE = "SUCC-00001";
	public static final String RESPONSE_SUCCESS_MESSAGE = "Data Success";
	
	public static final String SOAP_NS     = "http://schemas.xmlsoap.org/wsdl/soap/";
	public static final String WSDL_NS	   = "http://schemas.xmlsoap.org/wsdl/";
	public static final String SOAP_ENV_NS = "http://schemas.xmlsoap.org/soap/envelope/";
	public static final String SERVER_IP   = "10.20.212.179"; //10.20.212.179
	public static final String LISTEN_PORT = "8080";
	
	public static final String MOCK_URL_PREFIX = "http://" + SERVER_IP + ":" + LISTEN_PORT + "/mockserver/mockservlet/";

	
	public static enum Error{
		ERROR_DATA_500("ERR-00000", "Oops something when wrong"),
		ERROR_DATA_ALREADY_EXIST("ERR-00001", "Data Already Exist"),
	    ERROR_DATA_NOT_FOUND("ERR-00002", "Data Not Found");
	
		private String code;
	    private String errorMessage;
	        
		private Error(String code, String errorMessage) {
			this.code = code;
			this.errorMessage = errorMessage;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	    
	}
}
