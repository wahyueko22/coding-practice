package bca.wsdl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


//import mockserver.DBconnection.MySQLConnection;
//https://stackoverflow.com/questions/15940234/how-to-do-a-soap-web-service-call-from-java-class
//https://www.w3schools.com/xml/tempconvert.asmx?wsdl
public class MainSoapClient {
		
	public static String downloadWsdlContract(String url) throws IOException{
		 StringBuilder result 	 = new StringBuilder();
		 URL urlSrc 			 = new URL(url);
		 HttpURLConnection conn  = (HttpURLConnection) urlSrc.openConnection();
		  
		 conn.setRequestMethod("GET");
		  
		 BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		 String line;
		 while ((line = rd.readLine()) != null) {
		    result.append(line);
		 }
		 rd.close();
		 System.out.println(result.toString());
		 return result.toString();
	}
	
	public static String callSoapWebService(String soapEndpointUrl, String soapAction, String request) {
		 	String response = "";
		 	String output = "";
		 	//MySQLConnection connection = new MySQLConnection();
		 	String query = "";
		 	try {
		 		System.out.println(soapEndpointUrl);
		 		System.out.println(soapAction);
		 		//System.out.println(code);
		 		System.out.println("request = " + request);
		 		
		 		//============================================new Create Soap Connection new Method==============================
		 		/*		 		String soapEndpointUrl = "http://10.20.200.142:9201/PlanSavingInquiry/PlanSavingInquiryPortTypeBndPort?wsdl";*/
		 				 		
		 		URL url = new URL(soapEndpointUrl);
				URLConnection conn = url.openConnection();
				HttpURLConnection httpConn = (HttpURLConnection)conn;
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				 
				byte[] buffer = new byte[request.length()];
				buffer = request.getBytes();
				bout.write(buffer);
				byte[] b = bout.toByteArray();
				
				/*String soapAction = "http://esb.bca.com/PlanSavingInquiry/ListAccount";*/
				// Set the appropriate HTTP parameters.
				
				
				httpConn.setRequestProperty("Content-Length",
				String.valueOf(b.length));
				httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
				httpConn.setRequestProperty("SOAPAction", soapAction);
				httpConn.setRequestMethod("POST");
				httpConn.setDoOutput(true);
				httpConn.setDoInput(true);
				OutputStream out = httpConn.getOutputStream();
				//Write the content of the request to the outputstream of the HTTP Connection.
				out.write(b);
				out.close();
				//Ready with sending the request.
				 
				//Read the response.
				InputStreamReader isr =
				new InputStreamReader(httpConn.getInputStream());
				BufferedReader in = new BufferedReader(isr);
				 
				//Write the SOAP message response to a String.
				while ((response = in.readLine()) != null) {
				output = response;
				}
				System.out.println("output = " + output);
		 		//==================================================================================================================		 		
		 		
	        } catch (Exception e) {
	            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
	            e.printStackTrace();
	            return "failed";
	        }
		 	finally {
		 		//out.close();
			}
	        return output;
	}
	
	public static void processData(String wsdlXml, String request) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document wsdlDoc = builder.parse(new InputSource(new StringReader(wsdlXml)));
		Element rootElement = wsdlDoc.getDocumentElement();

		String targetNamespace = rootElement.getAttribute("targetNamespace");
		System.out.println("Target namespace : " + targetNamespace);

		// Get info about the operation
		factory.setNamespaceAware(true);
		DocumentBuilder inputDocBuilder = factory.newDocumentBuilder();

		Document input = inputDocBuilder.parse(new InputSource(new StringReader(request)));
		NodeList nl = input.getElementsByTagNameNS(targetNamespace, "*");
		String inputOpr;
		if (nl.item(0) != null)
			inputOpr = nl.item(0).getLocalName();
		else {
			String namespace = WsdlUtil.getNsNotEqual(request, Constant.SOAP_ENV_NS).get(0);
			nl = input.getElementsByTagNameNS(namespace, "*");
			inputOpr = nl.item(0).getLocalName();
		}
		System.out.println("inputOpr : " + inputOpr);
		
		String soapNsPrefix = WsdlUtil.getNsPrefix(wsdlXml, Constant.SOAP_NS);
		System.out.println("soapNsPrefix : " + soapNsPrefix);
		Element locationElem = WsdlUtil.getElemWithTag(soapNsPrefix + ":address", rootElement);
		String mockName = "iPay/TrxInquiry/INSTALLMENT";
		String locationUrl = Constant.MOCK_URL_PREFIX + mockName + "/output";
		WsdlUtil.updateElemAttr(locationElem, "location", locationUrl);
		
		System.out.println("locationUrl : " + locationUrl);
		
		String mockedContract = WsdlUtil.domToString(wsdlDoc);
		
		System.out.println("Mocked contract : " + mockedContract);
		
	}
	
	public static void testDoc(String xml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc = builder.parse(new InputSource(new StringReader(xml)));
		Element rootElement = doc.getDocumentElement();

		String targetNamespace = rootElement.getAttribute("targetNamespace");
		System.out.println("Target namespace : " + targetNamespace);
	    
		doc.getDocumentElement().normalize();

        System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
        
       // NodeList nList     = doc.getElementsByTagNameNS(targetNamespace, "*");
        NodeList nList = doc.getElementsByTagName("*");
        for (int i = 0; i < nList.getLength(); i++) {
        	Node nNode = nList.item(i);
        	
        	System.out.println("\nCurrent Element: " + nNode.getNodeName());
        }
	}
	
	//https://weblog.west-wind.com/posts/2017/feb/12/empty-soapactions-in-asmx-web-services
	
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		//wsdl calculator
		//http://calculator-webservice.mybluemix.net/calculator?wsdl
		//https://www.w3schools.com/xml/tempconvert.asmx?wsdl
		String soapEndpointUrl = "https://www.w3schools.com/xml/tempconvert.asmx?wsdl";
		String soapAction = "https://www.w3schools.com/xml/CelsiusToFahrenheit";
		String request = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:myNamespace=\"https://www.w3schools.com/xml/\">\r\n" + 
				"                <SOAP-ENV:Header/>\r\n" + 
				"                <SOAP-ENV:Body>\r\n" + 
				"                    <myNamespace:CelsiusToFahrenheit>\r\n" + 
				"                        <myNamespace:Celsius>100</myNamespace:Celsius>\r\n" + 
				"                    </myNamespace:CelsiusToFahrenheit>\r\n" + 
				"                </SOAP-ENV:Body>\r\n" + 
				"            </SOAP-ENV:Envelope>";
		//System.out.println("request = " + request);
		WsdlData wsdlData = new WsdlData();
		String strWsdl = wsdlData.getWsdlData();
		callSoapWebService(soapEndpointUrl, soapAction, request);
		
		// untuk download wsdl dari sebuah URL
		//downloadWsdlContract(soapEndpointUrl);  
		
		//processData(strWsdl, request);
		//testDoc(request);
		
		String reqCurr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.oorsprong.org/websamples.countryinfo\">\r\n" + 
				"   <soapenv:Header/>\r\n" + 
				"   <soapenv:Body>\r\n" + 
				"      <web:CountryCurrency>\r\n" + 
				"         <web:sCountryISOCode>IDR</web:sCountryISOCode>\r\n" + 
				"      </web:CountryCurrency>\r\n" + 
				"   </soapenv:Body>\r\n" + 
				"</soapenv:Envelope>";
		String urlCurr = "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?wsdl";
		//callSoapWebService(urlCurr, "GET", reqCurr);
	}

}
