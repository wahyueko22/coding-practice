package bca.wsdl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WsdlUtil {
	private static Logger logger = LoggerFactory.getLogger(WsdlUtil.class);
		
	public static String getUrl(String url) {
		String[] arrA = url.split(Constant.spliterUrl);
		return arrA[1];
	}
	
	public static String dateTimeToString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.defaultFormatTimeStamp);
		return dateFormat.format(date);
	}
	
	public static String dateToString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.defaultFormatDt);
		return dateFormat.format(date);
	}
	
	/* mengubah object to string json
	 * @param obj adalah sembarang objek turunan dari Object
	 */
	public static String printObjectJson(Object jsonObject) {
		if(jsonObject == null)
			return "";
		ObjectMapper Obj = new ObjectMapper(); 
		String jsonStr = "";
        try { 
            // get Oraganisation object as a json string 
        	jsonStr = Obj.writeValueAsString(jsonObject); 
        } 
        catch (Exception e) { 
          //
        } 
        return jsonStr;
	}
		

	public static String getNsPrefix(String xml, String namespace){
		try {
			XMLStreamReader reader = XMLInputFactory.newFactory().createXMLStreamReader(new StringReader(xml));				
			while(reader.hasNext()){
				int event = reader.next();
				if(XMLStreamConstants.START_ELEMENT == event){
					if(reader.getNamespaceCount() > 0){
						for(int i = 0 ; i < reader.getNamespaceCount() ; i++){
							String nsUri = reader.getNamespaceURI(i);
							System.out.println("Ns URI : " + nsUri);
							if(nsUri.equals(namespace)){
								if(reader.getNamespacePrefix(i) != null)
									return reader.getNamespacePrefix(i);
								else 
									return "";
							}
						}
					}
				}
			}		
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<String> getNsNotEqual(String xml, String nsNotToSearch){
		try {
			ArrayList<String> arr = new ArrayList<String>();
			XMLStreamReader reader = XMLInputFactory.newFactory().createXMLStreamReader(new StringReader(xml));				
			while(reader.hasNext()){
				int event = reader.next();
				if(XMLStreamConstants.START_ELEMENT == event){
					if(reader.getNamespaceCount() > 0){
						for(int i = 0 ; i < reader.getNamespaceCount() ; i++){
							String nsUri = reader.getNamespaceURI(i);
							System.out.println("Ns URI : " + nsUri);
							if(!nsUri.equals(nsNotToSearch)){
								arr.add(nsUri);
							}
						}
					}
				}
			}
			return arr;
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getTargetNsFromRequest(String soapRequest){
		String targetNamespace = ""; 
		try {
			XMLStreamReader reader = XMLInputFactory.newFactory().createXMLStreamReader(new StringReader(soapRequest));				
			while(reader.hasNext()){
				int event = reader.next();
				if(XMLStreamConstants.START_ELEMENT == event){
					if(reader.getNamespaceCount() > 0){
						for(int i = 0 ; i < reader.getNamespaceCount() ; i++){
							String nsUri = reader.getNamespaceURI(i);
							if(!nsUri.equals(Constant.SOAP_ENV_NS)){
								targetNamespace = nsUri;
							}
						}
					}
				}
			}		
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}
		return targetNamespace;
	}
	
	public static String downloadWsdlContract(String url) throws IOException{
		 StringBuilder result 	 = new StringBuilder();
		 URL urlSrc 			 = new URL(url);
		 HttpURLConnection conn  = (HttpURLConnection) urlSrc.openConnection();
		  
		 conn.setRequestMethod("GET");
		  
		 BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		 String line;
		 while ((line = rd.readLine()) != null) {
		    result.append(line);
		 }
		 rd.close();
		 return result.toString();
	}
	
	 public static String getWsdlServiceLocaction(String tagName, Element element) {
		 NodeList list = element.getElementsByTagName(tagName);
	     if (list != null && list.getLength() > 0) {
	    	 return list.item(0).getAttributes().getNamedItem("location").getNodeValue();          
	     }

	     return null;
	 }
	 
	 public static Element getElemWithTag(String tagName, Element element){
		NodeList list = element.getElementsByTagName(tagName);
	    if (list != null && list.getLength() > 0) {
	        return (Element) list.item(0);     
	    }

	    return null;
	 }
	 
	public static void updateElemAttr(Element elem, String attrName, String val){
		elem.setAttribute(attrName, val);
	}
	
	public static String domToString(Document doc) throws TransformerException{
		TransformerFactory tf   = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		String str = writer.getBuffer().toString().replaceAll("\n|\r", "");
		
		return str;
	}
	
	private static HashMap<String, String> getAllNamespaces(String xml){
		HashMap<String, String> nsMap = new HashMap<String, String>();
		try {
			XMLStreamReader reader = XMLInputFactory.newFactory().createXMLStreamReader(new StringReader(xml));				
			while(reader.hasNext()){
				int event = reader.next();
				if(XMLStreamConstants.START_ELEMENT == event){
					if(reader.getNamespaceCount() > 0){
						for(int i = 0 ; i < reader.getNamespaceCount() ; i++){
							nsMap.put(reader.getNamespacePrefix(i), reader.getNamespaceURI(i));
						}
					}
				}
			}
			return nsMap;
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<String> getPossibleDynamicTags(String serviceInput, String serviceOutput) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder 	   = factory.newDocumentBuilder();
		
		Document xmlInput  = builder.parse(new InputSource(new StringReader(serviceInput)));
		Document xmlOutput = builder.parse(new InputSource(new StringReader(serviceOutput)));
		
		//map namespaceprefix, namespaceuri
		HashMap<String, String> nsMapInput  = getAllNamespaces(serviceInput);
		HashMap<String, String> nsMapOutput = getAllNamespaces(serviceOutput);
		
		NodeList inputNl 		  		  = xmlInput.getElementsByTagName("*");
		//map [ns]:[tagname], [prefix]:[tagname]
		HashMap<String, String> inputTags = new HashMap<String, String>();
		
		for (int i=0 ; i < inputNl.getLength() ; i++){
	        // Get element
	        Element element = (Element)inputNl.item(i);
	        String elemName = element.getNodeName();
	     
	        //if the elem has namespace, concatenate with its namespace uri to ensure uniqueness
	        if(elemName.contains(":")){
	        	String nsPrefix = elemName.split(":")[0];
	        	
	        	if(nsMapInput.get(nsPrefix).equals("http://schemas.xmlsoap.org/soap/envelope/"))
	        		continue;
	        	
	        	String originalName = elemName;
	        	elemName 		    = nsMapInput.get(nsPrefix) + ":" + elemName.split(":")[1];
	        	inputTags.put(elemName, originalName);
	        }
	        else
	        	inputTags.put(elemName, elemName);
	    }
		
		NodeList outputNl 				   = xmlOutput.getElementsByTagName("*");
		HashMap<String, String> outputTags = new HashMap<String, String>();
		
		for (int i=0 ; i< outputNl.getLength() ; i++){
	        //Get element
			Element element = (Element)outputNl.item(i);
	        String elemName = element.getNodeName();
	        
	        if(elemName.contains(":")){
	        	String nsPrefix = elemName.split(":")[0];
	        	
	        	if(nsMapOutput.get(nsPrefix).equals("http://schemas.xmlsoap.org/soap/envelope/"))
	        		continue;
	        	
	        	String originalName = elemName;
	        	elemName 			= nsMapOutput.get(nsPrefix) + ":" + elemName.split(":")[1];
	        	outputTags.put(elemName, originalName);
	        }
	        else
	        	outputTags.put(elemName, elemName);
	    }
		
		ArrayList<String> possibleDynamics = new ArrayList<String>();
		
		Iterator it = inputTags.entrySet().iterator();
		while(it.hasNext()){
			 Map.Entry pair = (Map.Entry)it.next();
			 String key		= pair.getKey().toString();
			 
			 if(outputTags.containsKey(key))
				 possibleDynamics.add(outputTags.get(key));
		}
		
		return possibleDynamics;
	}
	
	public static String handleDynamicFields(String request, String output, String fields) throws ParserConfigurationException, SAXException, IOException, TransformerException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder 	   = factory.newDocumentBuilder();
		
		Document xmlInput  = builder.parse(new InputSource(new StringReader(request)));
		Document xmlOutput = builder.parse(new InputSource(new StringReader(output)));
		
		String[] splittedFields = fields.split("#");
		for(int i = 0 ; i < splittedFields.length ; i++){
			String inputVal = xmlInput.getElementsByTagName(splittedFields[i]).item(0).getTextContent();
			Node outputNd	= xmlOutput.getElementsByTagName(splittedFields[i]).item(0);
			outputNd.setTextContent(inputVal);
		}
		
		return domToString(xmlOutput);
	}

}
