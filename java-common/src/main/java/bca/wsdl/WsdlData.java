package bca.wsdl;

public class WsdlData {
	public String getWsdlData() {
		String strWsld = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" + 
				"<wsdl:definitions\r\n" + 
				"	xmlns:tm=\"http://microsoft.com/wsdl/mime/textMatching/\"\r\n" + 
				"	xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"\r\n" + 
				"	xmlns:mime=\"http://schemas.xmlsoap.org/wsdl/mime/\"\r\n" + 
				"	xmlns:tns=\"https://www.w3schools.com/xml/\"\r\n" + 
				"	xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\"\r\n" + 
				"	xmlns:s=\"http://www.w3.org/2001/XMLSchema\"\r\n" + 
				"	xmlns:soap12=\"http://schemas.xmlsoap.org/wsdl/soap12/\"\r\n" + 
				"	xmlns:http=\"http://schemas.xmlsoap.org/wsdl/http/\" targetNamespace=\"https://www.w3schools.com/xml/\"\r\n" + 
				"	xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\">\r\n" + 
				"	<wsdl:types>\r\n" + 
				"		<s:schema elementFormDefault=\"qualified\" targetNamespace=\"https://www.w3schools.com/xml/\">\r\n" + 
				"			<s:element name=\"FahrenheitToCelsius\">\r\n" + 
				"				<s:complexType>\r\n" + 
				"					<s:sequence>\r\n" + 
				"						<s:element minOccurs=\"0\" maxOccurs=\"1\" name=\"Fahrenheit\" type=\"s:string\" />\r\n" + 
				"					</s:sequence>\r\n" + 
				"				</s:complexType>\r\n" + 
				"			</s:element>\r\n" + 
				"			<s:element name=\"FahrenheitToCelsiusResponse\">\r\n" + 
				"				<s:complexType>\r\n" + 
				"					<s:sequence>\r\n" + 
				"						<s:element minOccurs=\"0\" maxOccurs=\"1\" name=\"FahrenheitToCelsiusResult\" type=\"s:string\" />\r\n" + 
				"					</s:sequence>\r\n" + 
				"				</s:complexType>\r\n" + 
				"			</s:element>\r\n" + 
				"			<s:element name=\"CelsiusToFahrenheit\">\r\n" + 
				"				<s:complexType>\r\n" + 
				"					<s:sequence>\r\n" + 
				"						<s:element minOccurs=\"0\" maxOccurs=\"1\" name=\"Celsius\" type=\"s:string\" />\r\n" + 
				"					</s:sequence>\r\n" + 
				"				</s:complexType>\r\n" + 
				"			</s:element>\r\n" + 
				"			<s:element name=\"CelsiusToFahrenheitResponse\">\r\n" + 
				"				<s:complexType>\r\n" + 
				"					<s:sequence>\r\n" + 
				"						<s:element minOccurs=\"0\" maxOccurs=\"1\" name=\"CelsiusToFahrenheitResult\" type=\"s:string\" />\r\n" + 
				"					</s:sequence>\r\n" + 
				"				</s:complexType>\r\n" + 
				"			</s:element>\r\n" + 
				"			<s:element name=\"string\" nillable=\"true\" type=\"s:string\" />\r\n" + 
				"		</s:schema>\r\n" + 
				"	</wsdl:types>\r\n" + 
				"	<wsdl:message name=\"FahrenheitToCelsiusSoapIn\">\r\n" + 
				"		<wsdl:part name=\"parameters\" element=\"tns:FahrenheitToCelsius\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"FahrenheitToCelsiusSoapOut\">\r\n" + 
				"		<wsdl:part name=\"parameters\" element=\"tns:FahrenheitToCelsiusResponse\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"CelsiusToFahrenheitSoapIn\">\r\n" + 
				"		<wsdl:part name=\"parameters\" element=\"tns:CelsiusToFahrenheit\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"CelsiusToFahrenheitSoapOut\">\r\n" + 
				"		<wsdl:part name=\"parameters\" element=\"tns:CelsiusToFahrenheitResponse\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"FahrenheitToCelsiusHttpPostIn\">\r\n" + 
				"		<wsdl:part name=\"Fahrenheit\" type=\"s:string\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"FahrenheitToCelsiusHttpPostOut\">\r\n" + 
				"		<wsdl:part name=\"Body\" element=\"tns:string\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"CelsiusToFahrenheitHttpPostIn\">\r\n" + 
				"		<wsdl:part name=\"Celsius\" type=\"s:string\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:message name=\"CelsiusToFahrenheitHttpPostOut\">\r\n" + 
				"		<wsdl:part name=\"Body\" element=\"tns:string\" />\r\n" + 
				"	</wsdl:message>\r\n" + 
				"	<wsdl:portType name=\"TempConvertSoap\">\r\n" + 
				"		<wsdl:operation name=\"FahrenheitToCelsius\">\r\n" + 
				"			<wsdl:input message=\"tns:FahrenheitToCelsiusSoapIn\" />\r\n" + 
				"			<wsdl:output message=\"tns:FahrenheitToCelsiusSoapOut\" />\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"		<wsdl:operation name=\"CelsiusToFahrenheit\">\r\n" + 
				"			<wsdl:input message=\"tns:CelsiusToFahrenheitSoapIn\" />\r\n" + 
				"			<wsdl:output message=\"tns:CelsiusToFahrenheitSoapOut\" />\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"	</wsdl:portType>\r\n" + 
				"	<wsdl:portType name=\"TempConvertHttpPost\">\r\n" + 
				"		<wsdl:operation name=\"FahrenheitToCelsius\">\r\n" + 
				"			<wsdl:input message=\"tns:FahrenheitToCelsiusHttpPostIn\" />\r\n" + 
				"			<wsdl:output message=\"tns:FahrenheitToCelsiusHttpPostOut\" />\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"		<wsdl:operation name=\"CelsiusToFahrenheit\">\r\n" + 
				"			<wsdl:input message=\"tns:CelsiusToFahrenheitHttpPostIn\" />\r\n" + 
				"			<wsdl:output message=\"tns:CelsiusToFahrenheitHttpPostOut\" />\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"	</wsdl:portType>\r\n" + 
				"	<wsdl:binding name=\"TempConvertSoap\" type=\"tns:TempConvertSoap\">\r\n" + 
				"		<soap:binding transport=\"http://schemas.xmlsoap.org/soap/http\" />\r\n" + 
				"		<wsdl:operation name=\"FahrenheitToCelsius\">\r\n" + 
				"			<soap:operation soapAction=\"https://www.w3schools.com/xml/FahrenheitToCelsius\" style=\"document\" />\r\n" + 
				"			<wsdl:input>\r\n" + 
				"				<soap:body use=\"literal\" />\r\n" + 
				"			</wsdl:input>\r\n" + 
				"			<wsdl:output>\r\n" + 
				"				<soap:body use=\"literal\" />\r\n" + 
				"			</wsdl:output>\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"		<wsdl:operation name=\"CelsiusToFahrenheit\">\r\n" + 
				"			<soap:operation soapAction=\"https://www.w3schools.com/xml/CelsiusToFahrenheit\" style=\"document\" />\r\n" + 
				"			<wsdl:input>\r\n" + 
				"				<soap:body use=\"literal\" />\r\n" + 
				"			</wsdl:input>\r\n" + 
				"			<wsdl:output>\r\n" + 
				"				<soap:body use=\"literal\" />\r\n" + 
				"			</wsdl:output>\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"	</wsdl:binding>\r\n" + 
				"	<wsdl:binding name=\"TempConvertSoap12\" type=\"tns:TempConvertSoap\">\r\n" + 
				"		<soap12:binding transport=\"http://schemas.xmlsoap.org/soap/http\" />\r\n" + 
				"		<wsdl:operation name=\"FahrenheitToCelsius\">\r\n" + 
				"			<soap12:operation soapAction=\"https://www.w3schools.com/xml/FahrenheitToCelsius\" style=\"document\" />\r\n" + 
				"			<wsdl:input>\r\n" + 
				"				<soap12:body use=\"literal\" />\r\n" + 
				"			</wsdl:input>\r\n" + 
				"			<wsdl:output>\r\n" + 
				"				<soap12:body use=\"literal\" />\r\n" + 
				"			</wsdl:output>\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"		<wsdl:operation name=\"CelsiusToFahrenheit\">\r\n" + 
				"			<soap12:operation soapAction=\"https://www.w3schools.com/xml/CelsiusToFahrenheit\" style=\"document\" />\r\n" + 
				"			<wsdl:input>\r\n" + 
				"				<soap12:body use=\"literal\" />\r\n" + 
				"			</wsdl:input>\r\n" + 
				"			<wsdl:output>\r\n" + 
				"				<soap12:body use=\"literal\" />\r\n" + 
				"			</wsdl:output>\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"	</wsdl:binding>\r\n" + 
				"	<wsdl:binding name=\"TempConvertHttpPost\" type=\"tns:TempConvertHttpPost\">\r\n" + 
				"		<http:binding verb=\"POST\" />\r\n" + 
				"		<wsdl:operation name=\"FahrenheitToCelsius\">\r\n" + 
				"			<http:operation location=\"/FahrenheitToCelsius\" />\r\n" + 
				"			<wsdl:input>\r\n" + 
				"				<mime:content type=\"application/x-www-form-urlencoded\" />\r\n" + 
				"			</wsdl:input>\r\n" + 
				"			<wsdl:output>\r\n" + 
				"				<mime:mimeXml part=\"Body\" />\r\n" + 
				"			</wsdl:output>\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"		<wsdl:operation name=\"CelsiusToFahrenheit\">\r\n" + 
				"			<http:operation location=\"/CelsiusToFahrenheit\" />\r\n" + 
				"			<wsdl:input>\r\n" + 
				"				<mime:content type=\"application/x-www-form-urlencoded\" />\r\n" + 
				"			</wsdl:input>\r\n" + 
				"			<wsdl:output>\r\n" + 
				"				<mime:mimeXml part=\"Body\" />\r\n" + 
				"			</wsdl:output>\r\n" + 
				"		</wsdl:operation>\r\n" + 
				"	</wsdl:binding>\r\n" + 
				"	<wsdl:service name=\"TempConvert\">\r\n" + 
				"		<wsdl:port name=\"TempConvertSoap\" binding=\"tns:TempConvertSoap\">\r\n" + 
				"			<soap:address location=\"http://www.w3schools.com/xml/tempconvert.asmx\" />\r\n" + 
				"		</wsdl:port>\r\n" + 
				"		<wsdl:port name=\"TempConvertSoap12\" binding=\"tns:TempConvertSoap12\">\r\n" + 
				"			<soap12:address location=\"http://www.w3schools.com/xml/tempconvert.asmx\" />\r\n" + 
				"		</wsdl:port>\r\n" + 
				"		<wsdl:port name=\"TempConvertHttpPost\" binding=\"tns:TempConvertHttpPost\">\r\n" + 
				"			<http:address location=\"http://www.w3schools.com/xml/tempconvert.asmx\" />\r\n" + 
				"		</wsdl:port>\r\n" + 
				"	</wsdl:service>\r\n" + 
				"</wsdl:definitions>\r\n" + 
				"";
		return strWsld;
	}
}
