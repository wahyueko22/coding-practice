package bca.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.json.JsonObject;
import javax.json.JsonValue;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainRest {

	public static String replaceResponse(String request, String response) {
		System.out.println("query from db: " + request);
		System.out.println("Print Input: " + response);
		if (request == null || "".equals(request))
			return response;

		Map<String, String> newMapValue = new HashMap<String, String>();
		Map<String, String> oldMapValue = new HashMap<String, String>();

		// ubah request ke json object
		JSONObject requestObj = new JSONObject(request);
		for (String key : requestObj.keySet()) {
			// based on you key types
			String keyStr = key;
			Object objectValue = requestObj.get(keyStr);
			String keyvalue = "";
			if (objectValue instanceof Integer) {
				keyvalue = objectValue.toString();
			} else if (objectValue instanceof String) {
				keyvalue = (String) objectValue;
			} else if (objectValue instanceof Boolean) {
				keyvalue = objectValue.toString();
			}
			// String keyvalue =(String) requestObj.get(keyStr);
			System.out.println("key req =  " + keyStr + "and req value =" + keyvalue);
			newMapValue.put(keyStr, keyvalue);
		}

		// mencari value / nilai dari response yang sama dg key request dan masukkan
		// dalam oldMapValue
		for (Map.Entry<String, String> entry : newMapValue.entrySet()) {
			String[] arr1 = response.split(",");
			for (int j = 0; j < arr1.length; j++) {
				// System.out.println("arr " + j + " = " + arr1[j]);
				if (arr1[j].contains(entry.getKey())) {
					String arr2[] = arr1[j].split(":");
					String str2 = arr2[arr2.length - 1];
					String arr3[] = str2.split("\"");
					if (arr3.length >= 2) {
						System.out.println(arr3[1]);
						oldMapValue.put(entry.getKey(), arr3[1]);
					} else {
						System.out.println(arr3[0]);
						oldMapValue.put(entry.getKey(), arr3[0]);
					}
				}
			}
		}

		for (Map.Entry<String, String> entry : oldMapValue.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}

		List<String> lsResult = new ArrayList<String>();
		String[] arr1 = response.split(",");
		// cari nilai yang mau direplace dengan nilai yang baru
		for (int j = 0; j < arr1.length; j++) {
			String temp = arr1[j];

			for (Map.Entry<String, String> entry : oldMapValue.entrySet()) {

				if (arr1[j].contains(entry.getKey())) {
					System.out.println(arr1[j] + " = " + entry.getKey());
					String strChange = arr1[j].replaceAll(entry.getValue(), newMapValue.get(entry.getKey()));
					temp = strChange;
					// jika nilai ditemukan lakukan lompat / keluar loop
					break;
				}
			}
			lsResult.add(temp);
		}

		String valRes = "";
		// kontruk kembali list of string ke dalam json string
		for (int i = 0; i < lsResult.size(); i++) {
			if (i != lsResult.size() - 1) {
				valRes += lsResult.get(i) + ",";
			} else {
				valRes += lsResult.get(i);
			}
		}

		System.out.println("value after : " + valRes);
		return valRes;
	}

	// parent = array ada object
	// anak = ada array ada object

	public static void loopingJson(String req, String res) {
		System.out.println("ori res : " + res);
		Map<String, Object> newMapValue = new HashMap<String, Object>();
		JSONObject requestObj = new JSONObject(req);
		for (String key : requestObj.keySet()) {
			// based on you key types
			String keyStr = key;
			Object objectValue = requestObj.get(keyStr);
			// String keyvalue =(String) requestObj.get(keyStr);
			System.out.println("key req =  " + keyStr + "and req value =" + objectValue);
			newMapValue.put(keyStr, objectValue);
		}
		
		String one = res.substring(0, 1);
		if ("[".equals(one)) {
			JSONArray jsonArray = new JSONArray(res);
			JSONArray jsonRet = new JSONArray();
			Iterator<Object> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArray,jsonRet,newMapValue);
				} else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArray,jsonRet, newMapValue);
				} else {
					jsonRet.put(next);
				}

			}
			System.out.println(jsonRet.toString());

		} else {
			JSONObject jsonRes = new JSONObject(res);
			JSONObject jsonOk = new JSONObject();
			for (String key : jsonRes.keySet()) {
				String keyStr = key;
				Object objectValue = jsonRes.get(keyStr);
				// System.out.println(objectValue.toString());
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonRes, jsonOk,newMapValue);
				} else if (objectValue instanceof JSONArray){
					extractJsonArray(keyStr, objectValue, jsonRes,jsonRes, newMapValue);
				}else {
					jsonOk.put(key, objectValue);
				}

			}
			System.out.println(jsonOk.toString());
		}

	}

	// jika anak adalah jsonobject dan parent adalah jsonarray
	public static void extractJsonObject(Object objValue, JSONArray parentJson, JSONArray newJson, 	Map<String, Object> newMapValue) {
		if (objValue instanceof JSONObject && parentJson instanceof JSONArray) {
			JSONObject jsonRes = new JSONObject(objValue.toString());
			JSONObject jsonOk = new JSONObject();
			newJson.put(jsonOk);
			for (String key : jsonRes.keySet()) {
				String keyStr = key;
				Object objectValue = jsonRes.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonRes, jsonOk, newMapValue);
				}else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonRes, jsonOk, newMapValue);
				}else {
					if(newMapValue.containsKey(key)) {
						jsonOk.put(key, newMapValue.get(key));
					}else
						jsonOk.put(key, objectValue);	
				}
			}
		}
	}

	// jika anak dan parent adalah ssama2 json object
	public static void extractJsonObject(String jsonKey, Object objValue, JSONObject parentJson, JSONObject newJson, Map<String, Object> newMapValue) {
		if (objValue instanceof JSONObject && parentJson instanceof JSONObject) {
			JSONObject jsonRes = new JSONObject(objValue.toString());
			JSONObject jsonOk = new JSONObject();
			newJson.put(jsonKey, jsonOk);
			for (String key : jsonRes.keySet()) {
				String keyStr = key;
				Object objectValue = jsonRes.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonRes, jsonOk, newMapValue);
				}
				if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonRes, jsonOk, newMapValue);
				} else {
					if(newMapValue.containsKey(key)) {
						jsonOk.put(key, newMapValue.get(key));
					}else
						jsonOk.put(key, objectValue);
				}
			}
		}
	}

	// jika anak adalah jsonArray dan parent adalah jsonarray
	public static void extractJsonArray(Object objValue, JSONArray parentJson, JSONArray newJson, 	Map<String, Object> newMapValue) {
		if (objValue instanceof JSONArray && parentJson instanceof JSONArray) {
			JSONArray jsonArray = new JSONArray(objValue.toString());
			JSONArray jsonRet = new JSONArray();
			newJson.put(jsonRet);
			Iterator<Object> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArray, jsonRet, newMapValue);
				}
				if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArray, jsonRet, newMapValue);
				} else {
					jsonRet.put(next);
				}
			}
		}
	}

	// jika anak adalah jsonArray dan parent adalah jsonObject
	public static void extractJsonArray(String jsonKey, Object objValue, JSONObject parentJson, JSONObject newJson, 	Map<String, Object> newMapValue ) {
		if (objValue instanceof JSONArray && parentJson instanceof JSONObject) {
			JSONArray jsonArray = new JSONArray(objValue.toString());
			JSONArray jsonRet = new JSONArray();
			newJson.put(jsonKey,jsonRet);
			Iterator<Object> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArray, jsonRet, newMapValue);
				}
				if (next instanceof JSONArray) {
					 extractJsonArray(next, jsonArray, jsonRet, newMapValue);
				} else {
					jsonRet.put(next);
				}
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String req = "{    \"name\":\"anton\",    \"salary\":\"100\",    \"age\":\"20\"}";
		String res = "{\"status\":\"success\",\"data\":{\"name\":null,\"salary\":null,\"age\":null,\"id\":73}}";
		// String res ="[\"satu\",\"dua\", 3,4,true,false]";
		// replaceResponse(req, res);
		//loopingJson(req, res);

		String req1 = "{\r\n" + " \"CardNumber\":\"6000\",\r\n" + " \"CustomerID\":\"3000\"\r\n" + "}";
		String res1 = "{\r\n" + "   \"ErrorSchema\":{\r\n" + "      \"ErrorCode\":\"ESB-00-000\",\r\n"
				+ "      \"ErrorMessage\":{\r\n" + "         \"English\":\"Success\",\r\n"
				+ "         \"Indonesian\":\"Berhasil\"\r\n" + "      }\r\n" + "   },\r\n" + "   \"OutputSchema\":{\r\n"
				+ "      \"CustomerID\":\"089503405311\",\r\n" + "      \"CardNumber\":\"6019001088888801\",\r\n"
				+ "      \"OTPCode\":\"379079\",\r\n" + "      \"ReferenceCode\":\"00102085\",\r\n"
				+ "      \"ExpiredParam\":\"3600\",\r\n" + "      \"ExpiredDate\":\"2019-12-18 14:28:29\"\r\n"
				+ "   }\r\n" + " }";
		// replaceResponse(req1, res1);
		loopingJson(req1, res1);
	}

}
