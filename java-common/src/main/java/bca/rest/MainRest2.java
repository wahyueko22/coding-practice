package bca.rest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainRest2 {

	// parent = array ada object
	// anak = ada array ada object
	public static void replaceResponse(String req, String res) {
		System.out.println("ori res : " + res);
		Map<String, Object> newMapValue = new HashMap<String, Object>();
		JSONObject requestObj = new JSONObject(req);
		String jsonResponse = "";
		for (String key : requestObj.keySet()) {
			// based on you key types
			String keyStr = key;
			Object objectValue = requestObj.get(keyStr);
			// String keyvalue =(String) requestObj.get(keyStr);
			System.out.println("key req =  " + keyStr + "and req value =" + objectValue);
			newMapValue.put(keyStr, objectValue);
		}

		String one = res.substring(0, 1);
		
		if ("[".equals(one)) {
			JSONArray jsonArrayInput = new JSONArray(res);
			JSONArray jsonArrayOutput = new JSONArray();
			Iterator<Object> iterator = jsonArrayInput.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArrayInput,jsonArrayOutput,newMapValue);
				} else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArrayInput,jsonArrayOutput, newMapValue);
				} else {
					jsonArrayOutput.put(next);
				}
			}
			jsonResponse =  jsonArrayOutput.toString();
		} else {
			JSONObject jsonObjectInput = new JSONObject(res);
			JSONObject jsonObjectOutput = new JSONObject();
			for (String key : jsonObjectInput.keySet()) {
				String keyStr = key;
				Object objectValue = jsonObjectInput.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else {
					if (newMapValue.containsKey(key)) {
						jsonObjectOutput.put(key, newMapValue.get(key));
					} else
						jsonObjectOutput.put(key, objectValue);
				}

			}
			jsonResponse =  jsonObjectOutput.toString();
		}
		System.out.println("output : " + jsonResponse);
	}

	// jika anak adalah jsonobject dan parent adalah jsonarray
	public static void extractJsonObject(Object childJson, JSONArray parentJson, JSONArray newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONObject && parentJson instanceof JSONArray) {
			JSONObject jsonObjectInput = new JSONObject(childJson.toString());
			JSONObject jsonObjectOutput = new JSONObject();
			newJson.put(jsonObjectOutput);
			for (String key : jsonObjectInput.keySet()) {
				String keyStr = key;
				Object objectValue = jsonObjectInput.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else {
					if (newMapValue.containsKey(key)) {
						jsonObjectOutput.put(key, newMapValue.get(key));
					} else
						jsonObjectOutput.put(key, objectValue);
				}
			}
		}
	}

	// jika anak dan parent adalah ssama2 json object
	public static void extractJsonObject(String jsonKey, Object childJson, JSONObject parentJson, JSONObject newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONObject && parentJson instanceof JSONObject) {
			JSONObject jsonObjectInput = new JSONObject(childJson.toString());
			JSONObject jsonObjectOutput = new JSONObject();
			newJson.put(jsonKey, jsonObjectOutput);
			for (String key : jsonObjectInput.keySet()) {
				String keyStr = key;
				Object objectValue = jsonObjectInput.get(keyStr);
				if (objectValue instanceof JSONObject) {
					extractJsonObject(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				}
				else if (objectValue instanceof JSONArray) {
					extractJsonArray(keyStr, objectValue, jsonObjectInput, jsonObjectOutput, newMapValue);
				} else {
					if (newMapValue.containsKey(key)) {
						jsonObjectOutput.put(key, newMapValue.get(key));
					} else
						jsonObjectOutput.put(key, objectValue);
				}
			}
		}
	}

	// jika anak adalah jsonArray dan parent adalah jsonarray
	public static void extractJsonArray(Object childJson, JSONArray parentJson, JSONArray newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONArray && parentJson instanceof JSONArray) {
			JSONArray jsonArrayInput = new JSONArray(childJson.toString());
			JSONArray jsonArrayOuput = new JSONArray();
			newJson.put(jsonArrayOuput);
			Iterator<Object> iterator = jsonArrayInput.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				}
				else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				} else {
					jsonArrayOuput.put(next);
				}
			}
		}
	}

	// jika anak adalah jsonArray dan parent adalah jsonObject
	public static void extractJsonArray(String jsonKey, Object childJson, JSONObject parentJson, JSONObject newJson,
			Map<String, Object> newMapValue) {
		if (childJson instanceof JSONArray && parentJson instanceof JSONObject) {
			JSONArray jsonArrayInput = new JSONArray(childJson.toString());
			JSONArray jsonArrayOuput = new JSONArray();
			newJson.put(jsonKey, jsonArrayOuput);
			Iterator<Object> iterator = jsonArrayInput.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof JSONObject) {
					extractJsonObject(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				}
				else if (next instanceof JSONArray) {
					extractJsonArray(next, jsonArrayInput, jsonArrayOuput, newMapValue);
				} else {
					jsonArrayOuput.put(next);
				}
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String req = "{    \"name\":\"anton\",    \"salary\":\"100\",    \"age\":\"20\",  \"titlek\":\"title-HHHHHH\"}";
		//String res = "{\"status\":\"success\",\"data\":{\"name\":null,\"salary\":null,\"age\":null,\"id\":73}}";
		// String res ="[\"satu\",\"dua\", 3,4,true,false]";
		String res = "{\"status\":\"success\",\"data\":{\"name\":null,\"salary\":null,\"age\":null,\"id\":73}}"; 
		// replaceResponse(req, res);
		replaceResponse(req, res);

		String req1 = "{\r\n" + " \"CardNumber\":\"Number200\",\r\n" + " \"CustomerID\":\"id300\"\r\n" + "}";
		String res1 = "{\r\n" + "   \"ErrorSchema\":{\r\n" + "      \"ErrorCode\":\"ESB-00-000\",\r\n"
				+ "      \"ErrorMessage\":{\r\n" + "         \"English\":\"Success\",\r\n"
				+ "         \"Indonesian\":\"Berhasil\"\r\n" + "      }\r\n" + "   },\r\n" + "   \"OutputSchema\":{\r\n"
				+ "      \"CustomerID\":\"089503405311\",\r\n" + "      \"CardNumber\":\"6019001088888801\",\r\n"
				+ "      \"OTPCode\":\"379079\",\r\n" + "      \"ReferenceCode\":\"00102085\",\r\n"
				+ "      \"ExpiredParam\":\"3600\",\r\n" + "      \"ExpiredDate\":\"2019-12-18 14:28:29\"\r\n"
				+ "   }\r\n" + " }";
		// replaceResponse(req1, res1);
		//replaceResponse(req1, res1);
	}

}