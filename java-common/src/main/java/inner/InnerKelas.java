package inner;

public class InnerKelas {
	
	InnerSatu  innerSatu;
	
	class InnerSatu{
		InnerKelas utama = new InnerKelas();
		public void printName() {
	    	System.out.println("inner Name");
	    }
		
		public void printInnerName() {
	    	System.out.println("printInnerName");
	    }
	}
	
    public void printName() {
    	System.out.println("Name");
    }
	
	public static void main(String[] args) {
		InnerKelas inn = new InnerKelas();
		inn.printName();
		inn.innerSatu.printInnerName();
	}
}
