package coba;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

//import mockserver.DBconnection.MySQLConnection;
//https://stackoverflow.com/questions/15940234/how-to-do-a-soap-web-service-call-from-java-class
//https://www.w3schools.com/xml/tempconvert.asmx?wsdl
public class MainSoapClient {
		
	public static void downloadWsdlContract(String url) throws IOException{
		 StringBuilder result 	 = new StringBuilder();
		 URL urlSrc 			 = new URL(url);
		 HttpURLConnection conn  = (HttpURLConnection) urlSrc.openConnection();
		  
		 conn.setRequestMethod("GET");
		  
		 BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		 String line;
		 while ((line = rd.readLine()) != null) {
		    result.append(line);
		 }
		 rd.close();
		 System.out.println(result.toString());
		 //return result.toString();
	}
	
	 public static String callSoapWebService(String soapEndpointUrl, String soapAction, String request) {
		 	String response = "";
		 	String output = "";
		 	//MySQLConnection connection = new MySQLConnection();
		 	String query = "";
		 	try {
		 		System.out.println(soapEndpointUrl);
		 		System.out.println(soapAction);
		 		//System.out.println(code);
		 		System.out.println(request);
		 		
		 		//============================================new Create Soap Connection new Method==============================
		 		/*		 		String soapEndpointUrl = "http://10.20.200.142:9201/PlanSavingInquiry/PlanSavingInquiryPortTypeBndPort?wsdl";*/
		 				 		
		 		URL url = new URL(soapEndpointUrl);
				URLConnection conn = url.openConnection();
				HttpURLConnection httpConn = (HttpURLConnection)conn;
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				 
				byte[] buffer = new byte[request.length()];
				buffer = request.getBytes();
				bout.write(buffer);
				byte[] b = bout.toByteArray();
				
				/*String soapAction = "http://esb.bca.com/PlanSavingInquiry/ListAccount";*/
				// Set the appropriate HTTP parameters.
				
				
				httpConn.setRequestProperty("Content-Length",
				String.valueOf(b.length));
				httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
				httpConn.setRequestProperty("SOAPAction", soapAction);
				httpConn.setRequestMethod("POST");
				httpConn.setDoOutput(true);
				httpConn.setDoInput(true);
				OutputStream out = httpConn.getOutputStream();
				//Write the content of the request to the outputstream of the HTTP Connection.
				out.write(b);
				out.close();
				//Ready with sending the request.
				 
				//Read the response.
				InputStreamReader isr =
				new InputStreamReader(httpConn.getInputStream());
				BufferedReader in = new BufferedReader(isr);
				 
				//Write the SOAP message response to a String.
				while ((response = in.readLine()) != null) {
				output = response;
				}
				System.out.println(output);
		 		//==================================================================================================================		 		
		 		
	        } catch (Exception e) {
	            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
	            e.printStackTrace();
	            return "failed";
	        }
		 	finally {
		 		//out.close();
			}
	        return output;
	    }

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		//https://www.w3schools.com/xml/tempconvert.asmx?wsdl
		String soapEndpointUrl = "https://www.w3schools.com/xml/tempconvert.asmx?wsdl";
		String soapAction = "https://www.w3schools.com/xml/CelsiusToFahrenheit";
		String request = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:myNamespace=\"https://www.w3schools.com/xml/\">\r\n" + 
				"                <SOAP-ENV:Header/>\r\n" + 
				"                <SOAP-ENV:Body>\r\n" + 
				"                    <myNamespace:CelsiusToFahrenheit>\r\n" + 
				"                        <myNamespace:Celsius>100</myNamespace:Celsius>\r\n" + 
				"                    </myNamespace:CelsiusToFahrenheit>\r\n" + 
				"                </SOAP-ENV:Body>\r\n" + 
				"            </SOAP-ENV:Envelope>";
		callSoapWebService(soapEndpointUrl, soapAction, request);
		
		//downloadWsdlContract(soapEndpointUrl);

	}

}
