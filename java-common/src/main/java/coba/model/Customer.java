package coba.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer_Type")
public class Customer {
	@XmlElement(name = "CUSTOMER_DESKRIPSI")
	private CustomerDesciption cusDesc;

	public CustomerDesciption getCusDesc() {
		return cusDesc;
	}

	public void setCusDesc(CustomerDesciption cusDesc) {
		this.cusDesc = cusDesc;
	}
}
