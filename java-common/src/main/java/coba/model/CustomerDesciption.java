package coba.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDesciption_Type")
public class CustomerDesciption {
	@XmlElement(name = "CUSTOMER_NAMA")
	private String customerName;
	@XmlElement(name = "CUSTOMER_ALIAS")
	private String customerAlias;
	@XmlElement(name = "CUSTOMER_BERTINDAK_SEBAGAI")
	private String customerBertindakSebagai;
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAlias() {
		return customerAlias;
	}
	public void setCustomerAlias(String customerAlias) {
		this.customerAlias = customerAlias;
	}
	public String getCustomerBertindakSebagai() {
		return customerBertindakSebagai;
	}
	public void setCustomerBertindakSebagai(String customerBertindakSebagai) {
		this.customerBertindakSebagai = customerBertindakSebagai;
	}
	
	
	
}
