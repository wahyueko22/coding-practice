package coba;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import coba.model.Customer;
import coba.model.Customers;

public class XmlToPojoCus {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Customers cus = new Customers();
		 try { 
			 File file = new File("customerObj.xml"); 
			 JAXBContext jaxbContext = JAXBContext.newInstance(Customers.class); 
			 Unmarshaller unmarshaller = jaxbContext.createUnmarshaller(); 
			 cus = (Customers) unmarshaller.unmarshal(file); 
			 Customer  d= (Customer) cus.getCustList().get(0);
			 System.out.println(d.getCusDesc().getCustomerName());
			 System.out.println(d.getCusDesc().getCustomerAlias());
		  } 
		 catch (JAXBException e) 
		 { // TODO Auto-generated catch block e.printStackTrace(); 
			 
		 }
		
	}
}
