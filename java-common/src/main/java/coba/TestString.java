package coba;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

//import org.json.JSONObject;
//https://beginnersbook.com/2014/07/hashtable-iterator-example-java/
public class TestString {
	public static void main(String[] s) throws Exception {
		int a = 45;
		String strNumber= Integer.toString(a);
		int res = 0;
		for(int k=0; k < strNumber.length(); k++) {
			res += Character.getNumericValue(strNumber.charAt(k));
		}
		res += a;
		System.out.println(res);
	}
}
